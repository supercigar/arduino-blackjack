#define initX 2
#define initY 2
#define xDivider 51 // initX+#chars*charWidth+padding = 2+8*6+1 = 51

void drawMain(struct player players[]) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.cp437(true); // Use typesetting code page 437.
  display.drawRect(0, 0, 128, 64, 1); // Draws 1 pixel wide border around the screen
  for (int i = 0; i < playerCount; i++) {
    // Calculate and declare cursor position for player
    int currentY = initY + i * 16;
    players[i].cursor[1] = currentY;
    display.setCursor(initX, initY + i * 16);

    // Write status in front of name
    if (players[i].status == 0) {
      display.write('?');
    } else {
      display.write(players[i].status);
    }
    display.write(' ');

    // Write name
    for (int c = 0; c < 6; c++) { // c maximum is 6 since this is chosen as max name length.
      display.write(players[i].Name[c]);
    }

    players[i].cursor[0] = xDivider + 1;
  }
  display.drawLine(xDivider, 0, xDivider, 64, 1); // Seperator line
  display.display();
}

void drawCardToHand(struct player *player, int value, bool hidden) {
  if (player->hand_size != 0) {
    int suits[4] = {3, 4, 5, 6};
    char tens[3] = {'K', 'Q', 'J'};
    display.setCursor(player->cursor[0], player->cursor[1]);
    //int value = player->hand[player->hand_size - 1];
    display.write(' ');
    if (!hidden) {
      switch (value) {
        case 10:          display.write(tens[random(0, 3)]); break;
        case 11:          display.write('A');                break;
        default:          display.write(value + 48);         break;
      }
      display.write(suits[random(0, 4)]);
    } else {
      display.write('?');
      display.write(8); // Figure that looks like backside of a card.
    }
    player->cursor[0] += 3 * 6;
  }
  display.display();
}
