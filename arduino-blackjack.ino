// HUSK CASE SWTICH

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void setup() {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  randomSeed(analogRead(A0));
  Serial.println("BEGIN!");
}

// card size: 4 bytes
struct card {
  int suit;
  int value;
};

// player size: 27 bytes
struct player {
  String Name;
  int money;
  int standPin;
  int hitPin;
  int status; // O = 'O', S = 'S', X = 'X', ? = '?', » = 175
  int hand[4];
  int hand_size;
  int cursor[2];
};

int stake = 20;
player players[2] = {{"Dealer"}, {"Andre.", 150, 2, 3}};
int playerCount = sizeof(players) / 27;

void loop() {
  drawMain(players);
  dealTable(players);
  Serial.println(players[1].hand_size);
  Serial.print(players[1].hand[0]);
  Serial.print(' ');
  Serial.println(players[1].hand[1]);
  Serial.println(calcHand(players[1]));

  delay(1000000000000000);
}
