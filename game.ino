#define blackjack 21

void deal(struct player *player, bool hidden) {
  int suit = random(3, 7);
  int value = random(1, 14);
  if (value > 10) { // Used to keep odds correct.
    value = 10;
  } else if (value == 1){
    value = 11;
  }
  player->hand[player->hand_size] = value;
  player->hand_size += 1;
  if (hidden) {
    drawCardToHand(player, value, true);
  } else {
    drawCardToHand(player, value, false);
  }
}

void dealTable(struct player players[]) {
  for (int i = 0; i < playerCount; i++) {
    deal(&players[i], false);
    if (players[i].Name == "Dealer") {
      deal(&players[i], true);
    } else {
      deal(&players[i], false);
    }
  }
}

int calcHand(struct player player) {
  int value = 0;
  for (int i = 0; i < player.hand_size; i++){
    value += player.hand[i];
  }
  if (value > blackjack){
    for (int i = 0; i < player.hand_size; i++){
      if (value > blackjack and player.hand[i] == 11){
        value -= 10;
      }
    }
  }
  return value;
}

/*
  void deal(struct player *player, bool silent, bool hidden) {
  card c;
  randomCard(&c);
  player->hand_value[player->hand_size] = c.value;
  player->hand_display += "|" + c.alias + "| ";
  player->hand_size += 1;
  if (not silent) {
    Serial.print("Card |");
    if (not hidden) {
      Serial.print(String(c.alias));
    } else {
      Serial.print(" x ");
    }
    Serial.print("| dealt to ");
    Serial.println(player->Name);
  }
  }

  void dealTable(struct player players[]) {
  for (int i = 0; i < playerCount; i++) {
    deal(&players[i], false, false);
    if (players[i].Name == "Dealer") {
      deal(&players[i], false, true);
    } else {
      deal(&players[i], false, false);
    }
  }
  }

  int calcHand(struct player p){
  int _value = 0;
  for (int i = 0; i < 21; i++){
    _value += p.hand_value[i];
  }
  return _value;
  }

  bool waitForResponse(struct player *p, bool silent) {
  if (not (silent)){
    Serial.print("Waiting for ");
    Serial.print(p->Name);
    Serial.println(" to hit or stand");
    Serial.print("Current hand: ");
    Serial.println(p->hand_display);
  }
  bool waiting = true;
  while (waiting) {
    if (digitalRead(p->standPin) == HIGH) {
      p->stand = true;
      return false;
    } else if (digitalRead(p->hitPin) == HIGH) {
      return true;
    }
  }
  }

  void resetRoundPlayer(struct player *p){
  p->stand = true;
  p->hand_size = 0;
  p->hand_display = "";
  for (int i; i < 21; i++){
    p->hand_value[i] = 0;
  }
  }

  void resetRound(struct player players[]){
  for (int i; i < playerCount; i++){
    resetRoundPlayer(&players[i]);
  }
  }
*/
